# Python Hello World

This project provides a minimal Python 3.10 development environment in a  Visual Studio Code Dev Container.

## Features

- Python 3.10 installed in a Docker container
- [Python Visual Studio Code extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python) (ms-python.python)

## Requires

* Docker
* VS Code
* VS Code extension "Remote - Containers"

## Using

1. Create a new file - name it with the `py` extension
2. Add code to the file
3. Click on the `Run Python File` button in the upper right of the window

## Notes

1. This is an example that is meant to show you a simple Python development environment.
2. You will likely want to install packages.

---
Copyright &copy; 2022 The HFOSSedu Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.